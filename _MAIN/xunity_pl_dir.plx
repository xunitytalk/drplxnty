version=1
background=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunitytalk%20playlist.png
title=[B][COLOR steelblue]X[/COLOR][COLOR white]UNITYTALK[/COLOR][/B] [COLOR gray]PLAYLIST DIRECTORY[/COLOR]
logo=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png


#directories

type=playlist
name=[COLOR steelblue][B]XUNITYTALK[/B][/COLOR] [COLOR white]Official Playlist[/COLOR]
thumb=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png
URL=https://bitbucket.org/xnty/drplxnty/raw/master/XMLs/pl_types.xml

type=playlist
name=[COLOR steelblue][B]Building[/B][/COLOR] [COLOR white]Playlists[/COLOR]
thumb=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png
URL=http://gdata.youtube.com/feeds/api/users/mefroxmedia/playlists

type=playlist
name=[COLOR steelblue][B]Community[/B][/COLOR] [COLOR white]Playlists[/COLOR]
thumb=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png
URL=https://bitbucket.org/xnty/drplxnty/raw/master/XMLs/com_list.xml

type=playlist
name=[COLOR steelblue][B]Featured[/B][/COLOR] [COLOR white]Content[/COLOR]
thumb=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png
URL=https://bitbucket.org/xnty/drplxnty/raw/master/XMLs/feature_list.xml

type=playlist
name=[COLOR steelblue][B]Third-Party[/B][/COLOR] [COLOR white]Playlists[/COLOR]
thumb=https://bytebucket.org/xnty/drplxnty/raw/master/images/xunity_pl_dir.png 
URL=https://bitbucket.org/xnty/drplxnty/raw/master/XMLs/third-parties.xml
